(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
    $('.modal').modal();

  }); // end of document ready
})(jQuery); // end of jQuery name space
